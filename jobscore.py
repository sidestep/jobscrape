import re
from termcolor import colored
from collections import Counter

BAD_TITLE_TERMS = {'sap', 'oracle', 'frontend', 'front-end'}
SEEK_TERMS = {'clojure', 'python', 'erlang'}
BAD_TERMS = {'java', 'php', 'ruby', 'scrum', 'safe', 'go', 'sap', 'oracle'}

def toregex(terms):
    return r'|'.join([f"\\b{t}\\b" for t in terms])

def count_terms(txt, terms):
    return dict(Counter([w.lower() for w in re.findall(toregex(terms), txt, flags=re.IGNORECASE)]))

def count_seek_terms(txt):
    return count_terms(txt, SEEK_TERMS)

def count_bad_terms(txt):
    return count_terms(txt, BAD_TERMS)

#def score_job(job_info):
#    bad_title = count_terms(job_info.title, BAD_TITLE_TERMS)
#    if bad_title:
#        return (-1, 'bad title', bad_title) 
#    seek_terms = count_terms(job_info.description, SEEK_TERMS)
#    if seek_terms:
#        return (1, 'seek terms', (seek_terms, count_terms(job_info.description, BAD_TERMS)))
#    bad_country = count_terms(job_info.place, BAD_COUNTRIES)
#    if bad_country:
#        return (-1, 'bad country', bad_country) 
#
def _highlight(match):
    color = 'green' if match.group().lower() in SEEK_TERMS else 'red'
    return colored(match.group(), color)

def highlight_terms(txt):
    return re.sub(toregex(SEEK_TERMS.union(BAD_TERMS)), _highlight, txt, flags=re.IGNORECASE)

#print("trusas" if count_terms("", BAD_TERMS) else "falsas")
#txt = "fsdf asdgfsdaf bla Clojure, Java, Javascript, python sdf pythonista"
#print(highlight_terms(txt))
