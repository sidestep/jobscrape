#! /bin/bash

Q='clojure'
LASTWEEK='qdr:w'
LASTDAY='qdr:d'

for site in "workable.com" "bamboohr.com" "greenhouse.io" "breezy.hr" "lever.co"
do
    firefox -new-tab -url "https://www.google.com/search?q=${Q}+site:${site}&tbs=${LASTWEEK}"
done

#Workable (https://workable.com)
#Bamboo HR (https://bamboohr.com)
#Greenhouse (https://greeenhouse.io)
#Breezy (https://breezy.hr)
#Lever (https://lever.co)
