import logging
from linkedin_jobs_scraper import LinkedinScraper
from linkedin_jobs_scraper.events import Events, EventData
from linkedin_jobs_scraper.query import Query, QueryOptions, QueryFilters
from linkedin_jobs_scraper.filters import RelevanceFilters, TimeFilters, TypeFilters, ExperienceLevelFilters, RemoteFilters
from termcolor import colored
import jobscore

logging.basicConfig(level = logging.INFO)

#'query', 'location', 'job_id', 'job_index', 'link', 'apply_link', 'title', 'company', 'place', 'description', 'description_html', 'date', 'seniority_level', 'job_function', 'employment_type', 'industries'

res_file = "results.txt"

def save_job(seek_terms, bad_terms, job_data):
    f = open(res_file, 'a')
    f.write('\n'.join(
        [f'> {seek_terms}/{bad_terms}',
            colored(job_data.title, attrs=['bold']),
            colored(job_data.company, 'blue', attrs=['bold']),
            job_data.place,
            job_data.link,
            ''.join(['-'] * 200),
            jobscore.highlight_terms(job_data.description),
            ''.join(['='] * 200),
            '\n']))
    f.close()

def on_data(data: EventData):
    seek_terms = jobscore.count_seek_terms(data.description)
    bad_terms = jobscore.count_bad_terms(data.description)
    print(data.company)
    print(f"{seek_terms}/{bad_terms}")
    if seek_terms:
        save_job(seek_terms, bad_terms, data)

def on_error(error):
    print('ERROR: ', error)

def on_end():
    print('END')

scraper = LinkedinScraper(
    chrome_executable_path='./chromium/chromedriver', # Custom Chrome executable path (e.g. /foo/bar/bin/chromedriver) 
    chrome_options=None,
    max_workers=1,  # How many threads will be spawned to run queries concurrently (one Chrome driver for each thread)
    slow_mo=1.3,  # Slow down the scraper to avoid 'Too many requests (429)' errors
)

# Add event listeners
scraper.on(Events.DATA, on_data)
scraper.on(Events.ERROR, on_error)
scraper.on(Events.END, on_end)

qoptions=QueryOptions(
    locations=['European Economic Area'],
    optimize=False,
    limit=100,
    filters=QueryFilters(relevance=RelevanceFilters.RECENT, time=TimeFilters.DAY))

queries = [
    Query(query='clojure', options=qoptions),
    Query(query='solution software digital technical architect', options=qoptions),
    Query(query='tech lead', options=qoptions)]


open(res_file, 'w').close()
scraper.run(queries)
